	from selenium import webdriver
	import time
	import re
	from datetime import timedelta, datetime
	#параметры
	days = 20 #количество допустимых дней с момента публикации
	acc_subsvriptions = 500 #количесвто допустимых дней с момента публикации
	publications = 10 #необходимый минимум публикаций

	from selenium.common.exceptions import NoSuchElementException
	from selenium.common.exceptions import StaleElementReferenceException

	today = datetime.now()
	# функция сущ элемента 
	def xpath_existence (url)
		try:
			browser.find_element_by_xpath(url)
			existance = 1
		except NoSuchElementException:
			existance = 0
		return existance
	browser = webdriver.Chrome("C:\\Program Files\\Sublime Text 3\\insta\\chromedriver.exe")

	#считывание из файла всех ссылок на пользователей
	f = open('C:\\Program Files\\Sublime Text 3\\insta\\persons_list.txt', 'r')
	file_list=[] 
	for line in f:
		file_list.append(line)
	f.close()

	#обработка ссылок
	
	# 1) публичный акк
	# 2) не более указанного Н подписоты
	# 3) без ссылок на рекламы
	# 4) нужно с фоткой
	# 5) необходимо не менее 10 публикаций
	# 6) последняя публикация Н-дней назад
	
	filtered_list = []
	i = 0
	j = 0

	for person in file_list:
		j +=1
		browser.get(person)
		time.sleep(0.4)

		# 1)
		element = "//section/main/div/div[2]/article[2]/div[1]/div/h2"
		if xpath_existence(element) == 1:
			try:
				if browser.find_element_by_xpath(element).text == "This account is Private" or "Это закрытый аккаунт":
					print(j, "Приватный аккаунт")

			except StaleElementReferenceException:
				print("Ошибка с кодом 1")

		# 2)
		element = "//section/main/div/ul/li[3]/span/span"
		if xpath_existence(element) == 0:
			print(j, "Ошибка в коде 2")	
			continue
		status = browser.find_element_by_xpath(element).text
		status = re.sub(r'\s', '', status) #удаление пробела из числа подписчиков
		if int(status) > acc_subsvriptions:
			print(j, "У аккаунта слишком много подписок")
			continue

		# 3)
		element = "//section/main/div/header/section/div[2]/a"
		if xpath_existence(element) ==1:
			print(j, 'Есть ссылка на сайт')
			continue

		# 4)
		element = "//section/main/div/ul/li[1]/span/span"
		if xpath_existence(element) == 0:
			print(j, "Ошибка в коде 4")	
			continue
		status = browser.find_element_by_xpath(element).text
		status = re.sub(r'\s', '', status)
		if int(status) < publications:
			print(j, 'У аккаунта сликшом много публикаций')
			continue

		# 5)
		element = "//section/main/div/header/div/div/span/img"
		if xpath_existence(element) == 0
			print(j, "Ошибка в коде 5")
			continue
		status = browser.find_element_by_xpath(element).get_attribute("src")
		if status.find(https://instagram.fvix9-1.fna.fbcdn.net/vp/de4dcee059fee103013d4b3bd0ed4568/5E695BF1/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fvix9-1.fna.fbcdn.net">") == -1
			print(j, "Профиль без аватарки")
			continue

		# 6)
		element = "//a[contains(@href, '/p/')]"
		if xpath_existence(element) == 0:
			print(j, "Ошибка в коде 5")
			continue
		status = browser.find_element_by_xpath(element).get_attribute('href')
		browser.get(status)
		post_date = browser.find_element_by_xpath("//time").get_attribute("datetime")
		year = int(post_date[0:4])
		month = int(post_date[5:7])
		day = int(post_date[8:10])
		post_date = datetime(year, month, day)
		period = today - post_date
		if period.days > days:
			print(j, "последняя публикация была слишком давно")
			continue

		filtered_list.append(person)
			print(j, "Добавлен новый пользователь", person)
			i+= 1


	f = open('C:\\Program Files\\Sublime Text 3\\insta\\filtered_persons_list.txt', 'w')
	for line in filtered_list:
		f.write(line)
	f.close()
	print("\nДобавлено", i, "пользователей")	

	browser.quit()