	from selenium import webdriver
	import time
	browser = webdriver.Chrome("C:\\Program Files\\Sublime Text 3\\insta\\chromedriver.exe")
	browser.get("https://www.instagram.com/")

	all = 300

	# Вход в аккаунт

	browser.get("https://www.instagram.com/accounts/login")

	browser.find_element_by_xpath("//section/main/div/article/div/div[1]/div/form/div[2]/div/label/input").send_keys("login")
	browser.find_element_by_xpath("//section/main/div/article/div/div[1]/div/form/div[3]/div/label/input").send_keys("pass")
	browser.find_element_by_xpath("//section/main/div/article/div/div[1]/div/form/div[4]/button").click()
	time.sleep(5)

	# Действия на сайте

	browser.get("https://www.instagram.com/natgeo/")
	time.sleep(3)
	browser.find_element_by_xpath("//section/main/div/article/div/div[1]/div/form/div[4]/button").click() #открытие списка подписчиков
	time.sleep(2)
	element = browser.find_element_by_xpath("/html/body/div[3]/div/div[2]")
	#начальная прокрутка
	browser.execute_script("argument[0].scrollTop = argument[0].scrollHeight/%s" %6, element)
	time.sleep(0.8)
	browser.execute_script("argument[0].scrollTop = argument[0].scrollHeight/%s" %4, element)
	time.sleep(0.8)
	browser.execute_script("argument[0].scrollTop = argument[0].scrollHeight/%s" %3, element)
	time.sleep(0.8)
	browser.execute_script("argument[0].scrollTop = argument[0].scrollHeight/%s" %2, element)
	time.sleep(0.8)
	browser.execute_script("argument[0].scrollTop = argument[0].scrollHeight/%s" %1.4, element)
	time.sleep(0.8)

	pers = [] #массив ссылок на пользователей
	t = 0.7 #ожидание после каждой прокрутки
	num_scroll = 0 # количество совершённых прокруток
	p = 0 #коэфф ожидания для 2000...пользователей

	#persons = browser.find_element_by_xpath("//div[@role='dialog']/div[2]/ul/div/li/div/div/div/div/a[@title]")
	#for i in range(20):
	#	pers.append(str(person[i].get_attribute('href')))

	while len(pers) < all:
		num_scroll += 1
		browser.execute_script("argument[0].scrollTop = argument[0].scrollHeight", element)

		if num_scroll % 10 == 0:
			print("!")
			#сохранение пользователей в массив
			persons = browser.find_element_by_xpath("//div[@role='dialog']/div[2]/ul/div/li/div/div/div/div/a[@title]")
			for i in range(len(persons)):
				pers.append(str(persons[i].get_attribute('href')))

		time.sleep(t)

		# ожидание 
		if (len(pers) > (2000 + 1000*p) ):
			print('\nWait_10_minutes.')
			time.sleep(60*10)
			p+=1

	#создание файла со списком пользователей
	f = open('C:\\Program Files\\Sublime Text 3\\insta\\persons_list.txt', 'w')
	for person in pers:
		f.write(person)
		f.write('\n')
	f.close()

	#закрытие браузера
	browser.quit()

	